#! /usr/bin/env python3

# RFLP_in-silico
# Copyright (C) 2020 Conde C - Branger M
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pandas
import argparse
import time
import json
import matplotlib.pyplot as plt
from pathlib import Path

from formula import RF

def show(input, prefix, outputdir, config, dpi, linewidth, format, sequence):
    output_directory = outputdir
    inputFiles = input

    # Initialization of variables
    ## Columns informations
    labels = []
    ISnumber = {}
    ## All bands positions
    y_values = []
    colors = []

    colNumber = range(len(inputFiles) + 1)

    # Boucle de remplissage des données à partir des fichiers
    # Loop - Filling data from files
    for genome in inputFiles:
        g = Path(genome)
        data = pandas.read_csv(g, sep="\t", names=None, header=None)[1].values
        labels.append(g.stem)
        ISnumber[g.stem] = len(data)
        y_values.append(data)
        colors.append("black")

    # Adding 2log to labels
    labels.insert(0, config["ladder"]["name"])
    y_values.insert(0, [RF(y) for y in config["ladder"]["size"]] +  [RF(y) for y in config["ladder_highlight"]["size"]])
    colors.insert(0, "royalblue")
    
    # Initialization of plt object
    fig, ax = plt.subplots()
    
    ax.eventplot(y_values, colors=colors, orientation="vertical", 
            lineoffsets=[i for i in colNumber], 
            linelengths=[0.65 for i in colNumber],
            linewidths=linewidth)

    if len(inputFiles) < 12:
        ax.set_xlim(-0.5, 12)
    
    # Display of ladder sizes
    for i, txt in enumerate(config["ladder"]["size"]):
        plt.text(-0.75, y_values[0][i], str(txt), size=config["ladder"]["fontsize"], color=config["ladder"]["text_color"], ha="center", fontfamily=config["labels"]["fontname"])
    for i, txt in enumerate(config["ladder_highlight"]["size"]):
        plt.text(-0.75, y_values[0][-1], str(txt), size=config["ladder"]["fontsize"], color=config["ladder_highlight"]["text_color"], ha="center", fontfamily=config["labels"]["fontname"])

    # Manage axis and legends
    ## Display ticks and labels
    ax.set_xticks(colNumber)
    ax.set_xticklabels(labels, horizontalalignment="left", rotation=30, fontsize=5, fontdict=config["labels"]["xticks"], fontfamily=config["labels"]["fontname"])
    
    ## Manage axis (hide)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.axes.get_yaxis().set_visible(False)
    
    ## Manage ticks
    ax.yaxis.set_ticks_position("left")
    ax.xaxis.set_ticks_position("top")
    
    # Find lowest displayed band
    min_y_values = []
    for y_value in y_values:
        min_y_values.append(min(y_value))
    min_y_value = min(min_y_values)

    # Display text
    plt.text(-1, min_y_value - 5, f"{sequence} copies", size=8, fontdict=config["labels"]["ISnumber"], fontfamily=config["labels"]["fontname"])
    for i, label in enumerate(labels[1:], 1):
        plt.text(i, min_y_value - 5, ISnumber[label], size=8, fontdict=config["labels"]["ISnumber"], fontfamily=config["labels"]["fontname"])
    
    # Display title
    plt.subplots_adjust(top=0.85)
    
    # Save the figure output
    plt.savefig(f"{output_directory}/{prefix}.{format}", orientation="portrait", dpi=dpi)
