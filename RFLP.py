#! /usr/bin/env python3

# RFLP_in-silico
# Copyright (C) 2020 Conde C - Branger M
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import argparse
import json
from pathlib import Path

from check_install import checkInstall
checkInstall(False)
import RFLP_Run
import RFLP_Show
import utils


def parse_argument():
    parser = argparse.ArgumentParser(description="")

    input_options = parser.add_argument_group("Input options")
    input_options.add_argument("-i","--input", nargs="*",
                        help="Assembled genome files in fasta or genbank format. Note that uncompleted genome would not work.")
    input_options.add_argument("-f","--file", type=str, default="",
                        help="File containing a list of assembled genome files in fasta or genbank format. Note that uncompleted genome would not work.")

    rflp_options = parser.add_argument_group("RFLP options")
    rflp_options.add_argument("-d", "--outputdir", type=str, default=Path.cwd(), 
                        help="Path to working directory. It will contain RFLP directory. [Current directory]")
    rflp_options.add_argument("--sequence", type=str, default="IS900",
                        help="Select an other insertion sequence. [IS900]")
    rflp_options.add_argument("--enzyme", type=str, default="",
                        help="Select an other restriction enzyme.")
    rflp_options.add_argument("--list_enzyme", action="store_true",
                        help="Print all available restriction enzyme and quit.")

    show_options = parser.add_argument_group("Visualization options")
    show_options.add_argument("-p", "--prefix", type=str, default="result",
                        help="Prefix name of output picture (default = results).")
    show_options.add_argument("-c", "--config", type=str, default="",
                        help="Path to the user configuration file.")
    show_options.add_argument("--dpi", type=int, default=300,
                        help="Image resolution in dpi (default = 300).")
    show_options.add_argument("--linewidth", type=float, default=0.85,
                        help="Width of displayed bands. To note, a value of 0.5 correspond to a very think line (default = 0.85).")
    show_options.add_argument("--format", type=str, default="png", choices=["png", "svg"],
                        help="Image output format (default = png).")

    args = parser.parse_args()

    if args.list_enzyme:
        for enzyme in utils.getRestrictionEnzyme():
            print(enzyme)
        sys.exit(0)

    if args.input and args.file:
        sys.exit("Use either -i or -f for input... Exiting.")
    elif args.file:
        with open(args.file) as inputs:
            args.input = inputs.read().splitlines()
    elif args.input:
        pass
    else:
        sys.exit("Use either -i or -f for input... Exiting.") 

    args.outputdir = Path(args.outputdir)
    if not args.outputdir.exists():
        args.outputdir.mkdir(parents=True)

    if not args.config:
        args.config = Path(__file__).resolve().parent / "config.json"
    else:
        args.config = Path(args.config).resolve()

    args.config = json.load(open(args.config))

    if args.enzyme:
        if args.enzyme not in utils.getRestrictionEnzyme():
            sys.exit(f"Restriction enzyme {args.enzyme} is not available... Exiting.")
    else:
        # Select default restriction enzyme
        try:
            args.enzyme = args.config["insertion_sequence"][args.sequence]
        except KeyError:
            sys.exit(f"The passed insertion sequence doesn't have an associated restriction enzyme.\nPlease provide a restriction enzyme by using the --enzyme <enzyme> option... Exiting.")

    return args


if __name__ == "__main__":

    args = parse_argument()

    tsv_files = []
    errors_returned = []
    
    output_directory = Path(f"{args.outputdir}/RFLP_{args.sequence}_{args.enzyme}")
    # Create output direcotry
    if not output_directory.exists():
        output_directory.mkdir(parents=True)
    
    # Checking if IF file exists 
    IS_file = Path(__file__).resolve().parent / "IS" / f"{args.sequence}.fasta"
    if not IS_file.exists():
        sys.exit(f"{IS_file} doesn't exists.")

    for inFile in args.input:
        state, tsv_file = RFLP_Run.rflp(inFile, IS_file, args.enzyme, output_directory, args.config)
        if state == "Validate":
            tsv_files.append(tsv_file)
        else:
            errors_returned.append([inFile, state])

    if len(tsv_files) > 0:
        RFLP_Show.show(tsv_files, args.prefix, output_directory, args.config, args.dpi, args.linewidth, args.format, args.sequence)
    else:
        print("No data to show.\n")

    errors_dict = {
        "WrongFormatFile": "Input file does not have a fasta/fa/fna extension.",
        "FileDoesntExists": "Input file does not exist !",
        "MultipleSequences": "Input file contains more than one sequence.",
        "NoIS": "No IS found in this sequence.",
        "NoRestrictionsFound": "Didn't find any restriction site.",
        "WrongGenbankFile": "Genbank file doesn't contains sequence.",
    }

    if len(errors_returned) > 0:
        print("\nError with file(s) :")
        for entry, state in errors_returned:
            print(f"{entry}\t{errors_dict[state]}")

