#! /usr/bin/env python3

# RFLP_in-silico
# Copyright (C) 2020 Conde C - Branger M
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#import os
#import sys
#import argparse
import subprocess
import pandas
from pathlib import Path
from Bio import SeqIO, SearchIO
from Bio.Restriction import RestrictionBatch
#from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
#from collections import Counter
from formula import RF
from utils import extract_fragment_sequence, genbank_to_fasta, get_format, getRestrictionEnzyme


#def getRestrictionEnzyme():
#    return AllEnzymes.as_string()
#
#
#def extract_fragment_sequence(start, end, sequence):
#    if end < start:
#        fragment = sequence[start:] + sequence[:end]
#    else:
#        fragment = sequence[start:end]
#    return fragment
#
#def genbank_to_fasta(path):
#    fasta = path.replace(os.path.splitext(path)[1], '.fna')
#    
#    if not os.path.exists(fasta):
#        genome_record = list(SeqIO.parse(path, "genbank"))
#        # Checking if genbank doesn't contain only N
#        if len(Counter(genome_record[0].seq[:5000]).most_common(2)) > 1:
#            SeqIO.write(genome_record, fasta, "fasta")
#        else:
#            fasta = False
#    return fasta
#
#def get_format(inFile):
#    with open(inFile) as unknown_file:
#        begin = unknown_file.read(5)
#        if begin == "LOCUS":
#            fileformat = "genbank"
#        elif begin[0] == ">":
#            fileformat = "fasta"
#        else:
#            fileformat = "unknown"
#    return fileformat

def rflp(input, IS_file, restrictEnzyme, output_directory, config):
    inputFile = Path(input)
    genome_dir = output_directory / inputFile.stem
    genome_dir.mkdir(exist_ok=True)

    # Checking file content (only one sequence)
    if not inputFile.exists():
        return("FileDoesntExists", False)

    # Checking file extension
    # Manage file if genbank provided
    if get_format(inputFile) == "genbank":
        newfasta = genbank_to_fasta(input)
        if newfasta:
            inputFile = Path(genbank_to_fasta(input))
        else:
            return("WrongGenbankFile", False)
    elif get_format(inputFile) != "fasta":
        return("WrongFormatFile", False)


    sequences = [record for record in SeqIO.parse(str(inputFile), "fasta") if len(record) >= 800000]
    if not (1 <= len(sequences) <= 4):
        return("MultipleSequences", False)

    # Getting IS size
    sequence_IS_length = len(SeqIO.read(IS_file, "fasta").seq)

    temporary_dir = genome_dir / 'temp'
    temporary_dir.mkdir(exist_ok=True)

    rflp_df = pandas.DataFrame(columns=f"{restrictEnzyme}_left\tIS_start\tIS_end\t{restrictEnzyme}_right\tFragment_size\tStrand\tChromosome\tIS".split("\t"))
    FASTA_fd = open(f"{str(genome_dir)}/RFLP_fragments.fasta", "w")
    GRAPH_fd = open(f"{str(genome_dir)}/{inputFile.stem}.tsv", "w")

    for chromosome in sequences:
        # Getting sequence
        #Fasta = lambda inputFile : SeqIO.read(inputFile, "fasta")
        #genome = Fasta(str(inputFile))
        genome_id = chromosome.id
        genome_seq = chromosome.seq

        temp_ch_file = temporary_dir / (f"seq_{genome_id}.fna")
        SeqIO.write(chromosome, temp_ch_file, "fasta")

        # Identification of restriction sites
        restrictionbatch = RestrictionBatch([restrictEnzyme])
        restrictions = {str(k): v for k, v in restrictionbatch.search(genome_seq, linear=False).items()} # convert keys from RestrictionType to str
        restrictions = restrictions[restrictEnzyme]

        if not restrictions:
            return("NoRestrictionsFound", False)

        # Identification of IS using blastn
        subprocess.run(["makeblastdb", "-in", str(temp_ch_file), "-dbtype", "nucl"], capture_output=True)
        subprocess.run(["blastn", "-query", str(IS_file), "-db", str(temp_ch_file), "-outfmt", "7", "-out", f"{str(temporary_dir)}/{genome_id}.blst", "-perc_identity", "90", "-evalue", "1e-100"])
        #subprocess.run(["blastn", "-query", str(IS_file), "-db", str(temp_ch_file), "-outfmt", "7", "-out", f"{str(temporary_dir)}/{genome_id}.blst"])

        # Getting IS positions
        IS = []
        for record in SearchIO.parse(f"{str(temporary_dir)}/{genome_id}.blst", "blast-tab", comments=True):
            for hsp in record.hsps:
                IS.append([hsp.hit_start, hsp.hit_end, hsp.hit_strand])
        if len(IS) == 0:
            continue #Next chromosome
            #return("NoIS", False)

        # Sorting positions by position on genome
        IS.sort(key=lambda x: x[0])

        data = []
        for value in IS:
            start, end, strand = value
            # Reverse start and end of the IS900 because BLAST result are parse with biopython 
            # which always give position in the same order
            if strand == -1:
                start, end = end, start

            # Search position of the nearest left restriction site
            restrictposF = [i for i in restrictions if i < start]
            if restrictposF:
                before = restrictposF[-1]
            else:
                before = 0

            # Search position of the nearest right restriction site
            restrictposR = [i for i in restrictions if i > start]
            if restrictposR:
                after = restrictposR[0]
            else:
                after = restrictions[0]

            if after < before :
                size = len(genome_seq) - before + after + 1
            else:
                size = after - before + 1
            data.append([before, value[0], value[1], after, size, value[2], genome_id])
            SeqIO.write(SeqRecord(extract_fragment_sequence(before, after, genome_seq), id=f"{genome_id}_{before}-{after}_{str(size)}bp", description=""), FASTA_fd, "fasta")
            y = RF(size)
            GRAPH_fd.write(f"{str(size)}\t{str(y)}\t{genome_id}\n")

        data = sorted(data, key=lambda x: x[0])
        [a.append(i) for i, a in enumerate(data, 1)]

        rflp_df_temp = pandas.DataFrame(data=data, columns=f"{restrictEnzyme}_left\tIS_start\tIS_end\t{restrictEnzyme}_right\tFragment_size\tStrand\tChromosome\tIS".split("\t"))
        rflp_df = pandas.concat([rflp_df, rflp_df_temp], ignore_index=True)

    #data = sorted(data, key=lambda x: x[4], reverse=True)
    #[a.append(i)  for i, a in enumerate(data, 1)]
    #data = sorted(data, key=lambda x: x[0])

    rflp_df = rflp_df.sort_values(by="Fragment_size", ascending=False)
    rflp_df["RFLP_band"] = [i for i, _ in enumerate(rflp_df["Fragment_size"].values, 1)]
    rflp_df = rflp_df.sort_values(by=["Chromosome", f"{restrictEnzyme}_left"])
    if len(rflp_df) == 0:
        return("NoIS", False)
    rflp_df.to_csv(f"{str(genome_dir)}/data.rflp", sep="\t", index=False)

    FASTA_fd.close()
    GRAPH_fd.close()

    return ("Validate", f"{str(genome_dir)}/{inputFile.stem}.tsv")
