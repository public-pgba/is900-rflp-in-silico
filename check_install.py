#! /usr/bin/env python3
import sys
import subprocess

def checkInstall(detailled=True):
    
    valid = []
    error = []
    
    if sys.version_info[:2] >= (3, 7):
        valid.append("Python version is ok.")
    else:
        error.append("!! Python version is not ok.")
        error.append("   You are using python "+sys.version[0:5]+" and 3+ is required.")
    
    reqs = subprocess.run([sys.executable, '-m', 'pip', 'freeze'], capture_output=True, encoding="utf8").stdout
    installed_packages = [r.split('==')[0] for r in reqs.split()]
    
    if "biopython" in installed_packages:
        valid.append("biopython installed.")
    else:
        error.append("!! biopython is not installed.")
    
    if "matplotlib" in installed_packages:
        valid.append("matplotlib installed.")
    else:
        error.append("!! matplotlib in not installed.")
    
    if "pandas" in installed_packages:
        valid.append("pandas installed.")
    else:
        error.append("!! pandas in not installed.")
    try:
        blast_version = [int(i) for i in subprocess.run("blastn -version".split(), capture_output=True, encoding="utf8").stdout.split("\n")[0].split(":")[1].strip(" +").split(".")]
        if blast_version >= [2,9,0]:
            valid.append("ncbi-blast+ installed.")
        else:
            error.append("ncbi-blast+ version is not good.")
    except FileNotFoundError:
        error.append("ncbi-blast+ not installed.")
   
        # Output
    if detailled:
        for value in valid:
            print(value)
        for value in error:
            print(value)

    if(len(error) > 0):
        print("\033[1m!! RFLP-insilico will not work well !!\033[0m")
        if not detailled:
            print("Run check_install.py for more details.")
        sys.exit(-1)
    else:
        print("\033[1mEverything is ready to use RFLP-insilico !\033[0m\n")

if __name__ == '__main__':
    if "check_install.py" in sys.argv[0] :
        checkInstall(True)
    else:
        checkInstall(False)
