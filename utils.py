import os
from collections import Counter
from Bio import SeqIO
from Bio.Restriction import AllEnzymes


def getRestrictionEnzyme():
    return AllEnzymes.as_string()


def extract_fragment_sequence(start, end, sequence):
    if end < start:
        fragment = sequence[start:] + sequence[:end]
    else:
        fragment = sequence[start:end]
    return fragment


def genbank_to_fasta(path):
    if not isinstance(path, str):
        path = str(path)

    fasta = path.replace(os.path.splitext(path)[1], '.fna')
    
    if not os.path.exists(fasta):
        genome_record = list(SeqIO.parse(path, "genbank"))
        # Checking if genbank doesn't contain only N
        if len(Counter(genome_record[0].seq[:5000]).most_common(2)) > 1:
            SeqIO.write(genome_record, fasta, "fasta")
        else:
            fasta = False
    return fasta


def get_format(inFile):
    with open(inFile) as unknown_file:
        begin = unknown_file.read(5)
        if begin == "LOCUS":
            fileformat = "genbank"
        elif begin[0] == ">":
            fileformat = "fasta"
        else:
            fileformat = "unknown"
    return fileformat

