# RFLP-*insilico*

	Copyright (C) 2021 Conde C - Branger M
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Overview
**RFLP-*in silico*** is a command-line software to simulate Restriction Fragment Length Polymorphism (RFLP) from completely assembled genomes on Insertion Sequences (like IS*900*) of _Mycobacterium avium_ subsp. _paratuberculosis_.
It is not able to run on uncompleted or contigs genomes due to lack of assembly of insertion sequences regions.

## Citations
Conde C. _et al._ 2021

## Licence
![Licence GPL GNUv3](GPL.png)

## How to use it ?
#### Depedencies
**We highly recommand to build and use our conda environment.**
	conda env create -f environment.yml 
or 


Python3 (v 3.7+)  
matplotlib (v 3.3.0)  
Biopython (v 1.77)  
Pandas (v 1.2.0)  
ncbi-blast+ (v 2.9.0+)

#### Download
Use git to download the most recent development tree.

	git clone https://forgemia.inra.fr/public-pgba/is900-rflp-in-silico.git

#### Install (if not using conda environment)
The softwares is ready-to-use until it's downloaded.
For Unix users, we recommand to add **RFLP-*in silico*** to your PATH environmental variable.  
You can check if every dependencies are installed by running 'check_install.py'.

	python3 check_install.py

#### Configuration
##### Adding a new IS
You have to edit the config.json file and register the couple IS-enzyme in the insertion_sequence field.
Do not forget to add IS sequence (.fasta) in the IS/ directory. The file name must be the name of the IS in insertion_sequences field.

##### Personnalize the output
You can change some solors and values of the output graph by editing config.json file or by creating your own file and provide it with -c option to RFLP.py.

## Usage
	conda activate is900-rflp-in-silico
### RFLP.py
	usage: RFLP.py [-h] [-i [INPUT [INPUT ...]]] [-f FILE] [-d OUTPUTDIR]
	               [--sequence SEQUENCE] [--enzyme ENZYME] [--list_enzyme]
	               [-p PREFIX] [-c CONFIG] [--dpi DPI] [--linewidth LINEWIDTH]
	               [--format {png,svg}]
	
	optional arguments:
	  -h, --help            show this help message and exit
	
	Input options:
	  -i [INPUT [INPUT ...]], --input [INPUT [INPUT ...]]
	                        Assembled genome files in fasta or genbank format.
	                        Note that uncompleted genome would not work.
	  -f FILE, --file FILE  File containing a list of assembled genome files in
	                        fasta or genbank format. Note that uncompleted genome
	                        would not work.
	
	RFLP options:
	  -d OUTPUTDIR, --outputdir OUTPUTDIR
	                        Path to working directory. It will contain RFLP
	                        directory. [Current directory]
	  --sequence SEQUENCE   Select an other insertion sequence. [IS900]
	  --enzyme ENZYME       Select an other restriction enzyme.
	  --list_enzyme         Print all available restriction enzyme and quit.
	
	Visualization options:
	  -p PREFIX, --prefix PREFIX
	                        Prefix name of output picture (default = results).
	  -c CONFIG, --config CONFIG
	                        Path to the user configuration file.
	  --dpi DPI             Image resolution in dpi (default = 300).
	  --linewidth LINEWIDTH
	                        Width of displayed bands. To note, a value of 0.5
	                        correspond to a very think line (default = 0.85).
	  --format {png,svg}    Image output format (default = png).

## Example
#### Basic example with paths
	python3 RFLP.py -i pathtodata/*.gbk

#### Basic example with file (allow you to set the order of genomes)
	python3 RFLP.py -f FILE

## FAQ
#### ERROR : myfile.gb Genbank file doesn't contains sequence
This error happens when you use a not fully genbank file. You must use a full genbank file which contains the sequence.
The software requires a sequence to process.

#### ERROR : findfont: Font family ['Liberation Serif'] not found. Falling back to DejaVu Sans.
This output is not an error. Matplotlib just signal that we use a font style that is not installed on your system.
This sometimes happens if you work in a container.
You can fix it with : 
```
sudo apt install msttcorefonts -qq
rm -rf ~/.cache/matplotlib
```

### Contributors
Created by **Cyril CONDE** and **Maxime BRANGER**.
With great advices of **Thierry COCHARD**.
Supervised by **Franck BIET** (PhD).

### Support
#### Reporting bugs
Please use the **Issues** panel to notify of any question or problem with the software.
#### Email
For any issues or questions, please use the [issue tracker](https://forgemia.inra.fr/public-pgba/is900-rflp-in-silico/-/issues) or by email to <maxime.branger@inrae.fr>.


